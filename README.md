# RSLisi

Welcome! 

RSLisi (« **Li**bre **Si**te web ») is the Republique Social Website Application.

Watch https://www.republiquesociale.eu/


## Author

Lead developer: Christian Pierre MOMON <christian.momon@devinsy.fr>

## Developing environment

RSLisi project uses Eclipse 4.29, Java 17, Git, Tomcat 10.1, Xidyn, Kiss4web.

### UTF-8 compliance settings
Set UTF-8 compliance when using Tomcat server directly:
- edit workspace/Servers/Tomcat v7.0 Server at localhost-config/server.xml
- add « URIEncoding="UTF-8" » to the 8080 connetor:
  <Connector connectionTimeout="20000" port="8080" protocol="HTTP/1.1" redirectPort="8443" URIEncoding="UTF-8"/>

Set UTF-8 compliance when using Tomcat server through Apache server (so using mod_jk):
- edit server.xml (on Debian, /etc/tomcat7/server.xml)
- add « URIEncoding="UTF-8" » to the AJP connetor:
  <Connector port="8009" protocol="AJP/1.3" redirectPort="8443" URIEncoding="UTF-8"/>

### Eclipse Mars tips
To get Tomcat server with Eclipse, install all JST packages.

## Build

## Change Log

Branch 0.1: Eclipse Mars, Java 8, Git, Tomcat 7, Xidyn, Kiss4web.

Branch 0.17: Eclipse 4.29, Java 17, Git, Tomcat 10.1, Xidyn, Kiss4web.


# License

RSLisi is released under the GNU AGPL license. 