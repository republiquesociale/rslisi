/**
 * Copyright (C) 2006-2023 Christian Pierre MOMON
 * 
 * This file is part of Kiss4web.
 * 
 * Kiss4web is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Kiss4web is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Kiss4web.  If not, see <http://www.gnu.org/licenses/>
 */
package eu.republiquesociale.rslisi;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.core.config.Configurator;
import org.apache.logging.log4j.core.config.DefaultConfiguration;
import org.slf4j.Logger;

import fr.devinsy.kiss4web.dispatcher.KissDispatcherUtils;

/**
 * Kiss4Web tests.
 */
public class FooSandbox
{
    private static Logger logger;

    static
    {
        // Initialize logger.
        org.apache.logging.log4j.Logger logger = null;

        Configurator.initialize(new DefaultConfiguration());
        Configurator.setRootLevel(Level.INFO);
        logger = LogManager.getLogger(FooSandbox.class.getName());
        logger.info("Enter");

        //
        logger.info("Set the log file format...");

        logger.info("... done.");

        logger.debug("Exit");
    }

    /**
     * Check.
     *
     * @param title
     *            the title
     * @param source
     *            the source
     * @param model
     *            the model
     * @return the string
     */
    public static String check(final String title, final StringBuffer source, final String model)
    {
        logger.info("test");
        String result;

        if (source.indexOf(model) == -1)
        {
            result = String.format("%-40s  ->  KO  <-", title) + "\nGet:\n" + source + "\nWaiting:\n" + model;

        }
        else
        {
            result = String.format("%-40s    [ OK ]  ", title);
        }

        //
        return (result);
    }

    /**
     * The main method.
     *
     * @param args
     *            the arguments
     */
    public static void main(final String[] args)
    {
        System.out.println("----------------------------");
        System.out.println(testCaller("/", "fr.devinsy.website"));
        System.out.println(testCaller("/good/", "fr.devinsy.website"));
        System.out.println(testCaller("/good/morning", "fr.devinsy.website"));
        System.out.println(testCaller("/good/day_day", "fr.devinsy.website"));
        System.out.println(testCaller("/good/day.xhtml", "fr.devinsy.website"));
    }

    /**
     * Test caller.
     *
     * @param pathInfo
     *            the path info
     * @param prefix
     *            the prefix
     * @return the string
     */
    public static String testCaller(final String pathInfo, final String prefix)
    {
        String result;

        result = "[" + pathInfo + "]=>[" + KissDispatcherUtils.pathInfoToClassName(pathInfo, prefix) + "]";

        //
        return (result);
    }
}
