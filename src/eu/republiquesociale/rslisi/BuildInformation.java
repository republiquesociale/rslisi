/**
 * Copyright (C) 2013-2023 Christian Pierre MOMON
 * 
 * This file is part of Kiss4web.
 * 
 * Kiss4web is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Kiss4web is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Kiss4web.  If not, see <http://www.gnu.org/licenses/>
 */
package eu.republiquesociale.rslisi;

import java.io.IOException;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class BuildInformation.
 */
public class BuildInformation
{
    private static final Logger logger = LoggerFactory.getLogger(BuildInformation.class);

    private String productName;
    private String majorRevision;
    private String minorRevision;
    private String buildNumber;
    private String buildDate;
    private String generator;
    private String author;

    /**
     * Instantiates a new builds the information.
     */
    public BuildInformation()
    {
        try
        {
            Properties build = new Properties();

            build.load(BuildInformation.class.getResource("/build_information.properties").openStream());

            //
            this.productName = build.getProperty("product.name", "n/a");
            this.majorRevision = build.getProperty("product.revision.major", "n/a");
            this.minorRevision = build.getProperty("product.revision.minor", "n/a");
            this.buildNumber = build.getProperty("product.revision.build", "n/a");
            this.buildDate = build.getProperty("product.revision.date", "n/a");
            this.generator = build.getProperty("product.revision.generator", "n/a");
            this.author = build.getProperty("product.revision.author", "n/a");

        }
        catch (IOException exception)
        {
            logger.error("Error loading the build.properties file: " + exception.getMessage(), exception);

            this.productName = "n/a";
            this.majorRevision = "n/a";
            this.minorRevision = "n/a";
            this.buildNumber = "n/a";
            this.buildDate = "n/a";
            this.generator = "n/a";
            this.author = "n/a";
        }
    }

    /**
     * Author.
     *
     * @return the string
     */
    public String author()
    {
        return this.author;
    }

    /**
     * Builds the date.
     *
     * @return the string
     */
    public String buildDate()
    {
        return this.buildDate;
    }

    /**
     * Builds the number.
     *
     * @return the string
     */
    public String buildNumber()
    {
        return this.buildNumber;
    }

    /**
     * Generator.
     *
     * @return the string
     */
    public String generator()
    {
        return this.generator;
    }

    /**
     * Major revision.
     *
     * @return the string
     */
    public String majorRevision()
    {
        return this.majorRevision;
    }

    /**
     * Minor revision.
     *
     * @return the string
     */
    public String minorRevision()
    {
        return this.minorRevision;
    }

    /**
     * Product name.
     *
     * @return the string
     */
    public String productName()
    {
        return this.productName;
    }

    /**
     * To string.
     *
     * @return the string
     */
    @Override
    public String toString()
    {
        String result;

        result = String.format("%s %s.%s.%s built on %s by %s", this.productName, this.majorRevision, this.minorRevision, this.buildNumber,
                this.buildDate, this.author);

        //
        return result;
    }

    /**
     * Version.
     *
     * @return the string
     */
    public String version()
    {
        String result;

        result = String.format("%s.%s.%s", this.majorRevision, this.minorRevision, this.buildNumber);

        //
        return result;
    }
}
