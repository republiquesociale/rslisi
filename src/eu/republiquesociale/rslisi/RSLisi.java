/**
 * Copyright (C) 2016-2023 Christian Pierre MOMON <christian.momon@devinsy.fr>
 * 
 * This file is part of RSLisi.
 * 
 * RSLisi is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * RSLisi is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with RSLisi.  If not, see <http://www.gnu.org/licenses/>
 */
package eu.republiquesociale.rslisi;

import fr.devinsy.kiss4web.Kiss4web;
import fr.devinsy.kiss4web.Kiss4web.Mode;
import jakarta.servlet.ServletContextEvent;
import jakarta.servlet.ServletContextListener;

/**
 * The Class RSLisi.
 */
public class RSLisi implements ServletContextListener
{
    private static class SingletonHolder
    {
        private static final RSLisi instance = new RSLisi();
    }

    private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(RSLisi.class);

    /**
     * {@inheritDoc}
     */
    @Override
    public void contextDestroyed(final ServletContextEvent sce)
    {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void contextInitialized(final ServletContextEvent event)
    {
        event.getServletContext();
    }

    /**
     * Instantiates a new RS lisi.
     */
    private RSLisi()
    {
        System.out.println("Current mode: " + Kiss4web.instance().getMode());
        logger.info("Current mode: " + Kiss4web.instance().getMode());
        Kiss4web.instance().setMode(Mode.OPEN);
        System.out.println("Current mode: " + Kiss4web.instance().getMode());
    }

    /**
     * Instance.
     *
     * @return the RS lisi
     */
    public static RSLisi instance()
    {
        return SingletonHolder.instance;
    }
}
