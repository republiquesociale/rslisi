/**
 * Copyright (C) 2016-2023 Christian Pierre MOMON <christian.momon@devinsy.fr>
 * 
 * This file is part of RSLisi.
 * 
 * RSLisi is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * RSLisi is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with RSLisi.  If not, see <http://www.gnu.org/licenses/>
 */
package website.editorial;

import java.io.IOException;

import fr.devinsy.xidyn.pages.Page;
import fr.devinsy.xidyn.pages.PageFactory;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import website.FatalView;
import website.charter.CharterPage;

/**
 * The Class Statuts_et_fonctionnement_xhtml.
 */
public class Statuts_et_fonctionnement_xhtml extends HttpServlet
{
    private static final long serialVersionUID = -3597088223059326129L;
    private static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(Statuts_et_fonctionnement_xhtml.class);

    /**
     * {@inheritDoc}
     */
    @Override
    public void doGet(final HttpServletRequest request, final HttpServletResponse response) throws IOException, ServletException
    {
        try
        {
            logger.debug("doGet starting…");

            // Get parameters.
            // ===============
            // Locale locale = rslisi.getUserLocale(request);
            // Long accountId = rslisi.getAuthentifiedAccountId(request,
            // response);

            // Use parameters.
            // ===============

            // Send response.
            // ==============
            Page page = PageFactory.instance().create("/website/editorial/statuts_et_fonctionnement.html");

            //
            Page charter = new CharterPage();
            charter.include("body_container", page);
            String html = charter.dynamize().toString();

            // Display page.
            response.setContentType("application/xhtml+xml; charset=UTF-8");
            response.getWriter().println(html);
        }
        catch (Exception exception)
        {
            FatalView.show(request, response, exception);
        }

        logger.debug("doGet done.");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void doPost(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException
    {
        doGet(request, response);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void init() throws ServletException
    {
    }
}
